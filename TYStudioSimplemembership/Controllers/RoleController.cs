﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TYStudio;

namespace TYStudio.UI.AdminTools.Controllers
{
    //[Authorize(Roles = "系统管理员")]
    [AllowAnonymous]
    [InitializeSimpleMembership]
    public class RoleController : Controller
    {
        private TYStudioMembershipContext db = new TYStudioMembershipContext();

        //
        // GET: /Role/

        
        public ActionResult Index()
        {
            return View(db.Roles.ToList());
        }

        //
        // GET: /Role/Details/5

        public ActionResult Details(int id = 0)
        {
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        //
        // GET: /Role/Create

        public ActionResult Create()
        {
            ViewData["PermissionSelectList"] = GetPermissionSelectList();
            return View();
        }

        //
        // POST: /Role/Create

        [HttpPost]
        public ActionResult Create(Role role, string[] permissions)
        {
            if (ModelState.IsValid)
            {
                if (db.Roles.SingleOrDefault(e => e.RoleName == role.RoleName) == null)
                {
                    Role currentRole = db.Roles.Add(role);
                    db.SaveChanges();
                    if (permissions != null)
                    {
                        foreach (var permissionId in permissions)
                        {
                            PermissionsInRoles pir = new PermissionsInRoles();
                            pir.RoleId = currentRole.RoleId;
                            pir.PermissionId = int.Parse(permissionId);
                            db.PermissionsInRoles.Add(pir);
                            db.SaveChanges();
                        }
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewData["PermissionSelectList"] = GetPermissionSelectList();
                    TempData["ErrorMessage"] = "角色名称已存在";
                }
            }

            return View(role);
        }

        //
        // GET: /Role/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        //
        // POST: /Role/Edit/5

        [HttpPost]
        public ActionResult Edit(Role role)
        {
            if (db.Roles.SingleOrDefault(e => e.RoleName == role.RoleName) == null)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(role).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "角色名称已存在";
            }
            return View(role);
        }

        public ActionResult EditPermission(int id = 0)
        {
            Role role = db.Roles.Single(e => e.RoleId == id);
            List<SelectListItem> permissionSelectList = GetPermissionSelectList();

            List<Permission> permissionList = db.PermissionsInRoles
                                    .Include("Permissions").Include("Roles")
                                    .Where(e => e.RoleId == id)
                                    .Select(e => e.Permission).ToList<Permission>();

            foreach (var item in permissionSelectList)
            {
                foreach (var permission in permissionList)
                {
                    if (item.Value == permission.PermissionId.ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

            ViewData["PermissionSelectList"] = permissionSelectList;
            return View(role);
        }

        public ActionResult Delete(int id)
        {
            Role role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private List<SelectListItem> GetPermissionSelectList()
        {
            List<SelectListItem> permissionList = new List<SelectListItem>();
            using (TYStudioMembershipContext db = new TYStudioMembershipContext())
            {
                foreach (var permission in db.Permissions)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = permission.PermissionId.ToString();
                    item.Text = permission.PermissionName;
                    permissionList.Add(item);
                }
            }
            return permissionList;
        }
    }
}